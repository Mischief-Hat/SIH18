# from scipy import misc
import tensorflow as tf
import numpy as np
import sys
import os
import glob
import cv2
import align.detect_face

if __name__ == '__main__':
	
	files = glob.glob('Dataset/candidates/*.jpg')
	minsize = 100 # minimum size of face
	threshold = [ 0.7, 0.7, 0.7 ]  # three steps's threshold
	factor = 0.709 # scale factor
	margin = 7

	print('Creating networks and loading parameters')
	with tf.Graph().as_default():
		gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.65)
		sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options, log_device_placement=False))
		with sess.as_default():
			pnet, rnet, onet = align.detect_face.create_mtcnn(sess, None)
	# cv2.namedWindow('faces',cv2.WINDOW_NORMAL)
	# cv2.resizeWindow('faces', 4048,3036)
	for k,file in enumerate(files):
		img = cv2.imread(os.path.expanduser(file))  	#/ misc.imread(file, mode='RGB')
		img = img[:,:,::-1] #BGR to RGB					#/ Don't use this if using scipy
		
		head, tail = os.path.split(file)
		output = cv2.imread(file)
		img_size = np.asarray(img.shape)[0:2]
		bounding_boxes, _ = align.detect_face.detect_face(img, minsize, pnet, rnet, onet, threshold, factor)
		
		for i in range(len(bounding_boxes)):
			det = np.squeeze(bounding_boxes[i,0:4])
			bb = np.zeros(4, dtype=np.int32)
			bb[0] = np.maximum(det[0], 0) #Left
			bb[1] = np.maximum(det[1], 0) #Top
			bb[2] = np.minimum(det[2], img_size[1]) #Right
			bb[3] = np.minimum(det[3], img_size[0])	#Bottom
			if(bb[2]-bb[0]<500):
				continue
	
			# cv2.rectangle(output,(bb[0],bb[1]),(bb[2],bb[3]),(0,255,0),2)
			faces = output[ bb[1]-margin:bb[3]+margin, bb[0]-margin:bb[2]+margin] #[top:bottom, left:right]			
			print(faces.shape)
			# faces = cv2.resize(faces,(160,160))  #Default is bilinear cv2.INTER_LINEAR
			
			dest = 'Dataset/candidates/'+'main-'+str(k+1000)+'.jpg'
			print(file)
			# cv2.imshow('faces',faces)
			# cv2.waitKey(0)
			cv2.imwrite(dest,faces)

		# dest = 'Dataset/res/'+tail
		# cv2.imwrite(dest,output)
		# print(dest)
		
		# cv2.imwrite(dest,output)
