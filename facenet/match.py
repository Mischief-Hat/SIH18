import cv2
import tensorflow as tf
import numpy as np
import sys
import os
import glob
import time
# import re
from tensorflow.python.platform import gfile
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
# import align.detect_face

# def get_model_filenames(model_dir):
# 	files = os.listdir(model_dir)
# 	meta_files = [s for s in files if s.endswith('.meta')]
# 	if len(meta_files)==0:
# 		raise ValueError('No meta file found in the model directory (%s)' % model_dir)
# 	elif len(meta_files)>1:
# 		raise ValueError('There should not be more than one meta file in the model directory (%s)' % model_dir)
# 	meta_file = meta_files[0]
# 	ckpt = tf.train.get_checkpoint_state(model_dir)
# 	if ckpt and ckpt.model_checkpoint_path:
# 		ckpt_file = os.path.basename(ckpt.model_checkpoint_path)
# 		return meta_file, ckpt_file

# 	meta_files = [s for s in files if '.ckpt' in s]
# 	max_step = -1
# 	for f in files:
# 		step_str = re.match(r'(^model-[\w\- ]+.ckpt-(\d+))', f)
# 		if step_str is not None and len(step_str.groups())>=2:

# 			step = int(step_str.groups()[1])
	# 		if step > max_step:
	# 			max_step = step
	# 			ckpt_file = step_str.groups()[0]
	# return meta_file, ckpt_file

# def load_model(model):
# 	# # Check if the model is a model directory (containing a metagraph and a checkpoint file)
# 	# #  or if it is a protobuf file with a frozen graph
# 	model_exp = os.path.expanduser(model)
# 	# if (os.path.isfile(model_exp)):
# 	# 	print('Model filename: %s' % model_exp)
# 	# 	with gfile.FastGFile(model_exp,'rb') as f:
# 	# 		graph_def = tf.GraphDef()
# 	# 		graph_def.ParseFromString(f.read())
# 	# 		tf.import_graph_def(graph_def, name='')

# 	print('Model directory: %s' % model_exp)
# 	meta_file, ckpt_file = get_model_filenames(model_exp)
	
# 	print('Metagraph file: %s' % meta_file)
# 	print('Checkpoint file: %s' % ckpt_file)

# 	saver = tf.train.import_meta_graph(os.path.join(model_exp, meta_file))
# 	saver.restore(tf.get_default_session(), os.path.join(model_exp, ckpt_file))

def load_model(model):
	# # Check if the model is a model directory (containing a metagraph and a checkpoint file)
	# #  or if it is a protobuf file with a frozen graph
    model_exp = os.path.expanduser(model)
    if (os.path.isfile(model_exp)):
        print('Model filename: %s' % model_exp)
        with gfile.FastGFile(model_exp,'rb') as f:
            graph_def = tf.GraphDef()
            graph_def.ParseFromString(f.read())
            tf.import_graph_def(graph_def, name='')
		
def load(path):
	files = glob.glob(os.path.join(path,'main-*.jpg'))
	img_list = []
	for k,file in enumerate(files):
		img = cv2.imread(file)
		img = img[:,:,::-1]
		img = normalize(img)
		img_list.append(img)
	images = np.stack(img_list)
	return  images, files

def normalize(x):
	mean = np.mean(x)
	std = np.std(x)
	std_adj = np.maximum(std, 1.0/np.sqrt(x.size))
	y = np.multiply(np.subtract(x, mean), 1/std_adj)
	return y 

if __name__ == '__main__':

	model = 'models/Inception_weights/20170512-110547.pb'
	images_detections, files_detections = load('Dataset/cropped')
	images_candidates, files_candidates = load('Dataset/candidates')
	# for i in range(len(images_candidates)):
	# 	cv2.imshow('window',images_candidates[i])
	# 	cv2.waitKey(0)

	with tf.Graph().as_default():

		with tf.Session() as sess:
			start = time.time()
			# Load the model
			load_model(model)
	
			# Get input and output tensors
			images_placeholder = tf.get_default_graph().get_tensor_by_name("input:0")
			embeddings = tf.get_default_graph().get_tensor_by_name("embeddings:0")
			phase_train_placeholder = tf.get_default_graph().get_tensor_by_name("phase_train:0")

			# Run forward pass to calculate embeddings
			feed_dict_detections = { images_placeholder: images_detections, phase_train_placeholder:False }
			feed_dict_candidates = { images_placeholder: images_candidates, phase_train_placeholder:False }
			emb_detections = sess.run(embeddings, feed_dict=feed_dict_detections)
			emb_candidates = sess.run(embeddings, feed_dict=feed_dict_candidates)
			stop = time.time()- start
			print('Time taken in seconds '+str(stop))

			with open('Dataset/encoding/Detections.txt','w') as f:
				for k,file in enumerate(files_detections):
					# head, tail = os.path.split(file)
					f.write(str(file)+ " ")
					for j in range(128):
						f.write(str(emb_detections[k,j])+ " ")
					f.write("\n")

			with open('Dataset/encoding/candidates.txt','w') as f:
				for k,file in enumerate(files_candidates):
					# head, tail = os.path.split(file)
					f.write(str(file)+ " ")
					for j in range(128):
						f.write(str(emb_candidates[k,j])+ " ")
					f.write("\n")

			for k in range(len(files_detections)):
				for j in range(len(files_candidates)):
					dist = np.sqrt(np.sum(np.square(np.subtract(emb_detections[k,:], emb_candidates[j,:]))))
					print('\n----------------------------')
					print(files_detections[k])
					print(files_candidates[j])
					if(dist>1):
						print('Different persons')
					else:
						print('Same persons')
					img_detection = images_detections[k]
					img_detection = img_detection[:,:,::-1]
					img_candidate = images_candidates[j]
					img_candidate = img_candidate[:,:,::-1]
					cv2.imshow('detections',img_detection)
					cv2.imshow('candidates',img_candidate)
					cv2.waitKey(0)
			
	# dist = np.sqrt(np.sum(np.square(np.subtract(emb[i,:], emb[j,:]))))



