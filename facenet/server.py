import socket
import threading
import time
import tensorflow as tf 
import numpy as np
import cv2
import glob
import time
from tensorflow.python.platform import gfile
import os
import sys
import align.detect_face
from sklearn.svm import SVC
import _pickle as pickle

# os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
# graph1 = tf.Graph()
# sess1 = tf.Session(config=tf.ConfigProto(gpu_options=tf.GPUOptions(per_process_gpu_memory_fraction=0.65), log_device_placement=False))

with tf.Graph().as_default():
	#Fastest config is on sess#1 with cpu
	# sess1 = tf.Session(config=tf.ConfigProto(device_count = {'GPU': 0}, log_device_placement=False))
	sess1 = tf.Session(config=tf.ConfigProto(gpu_options=tf.GPUOptions(per_process_gpu_memory_fraction=0.65), log_device_placement=False))
	with sess1.as_default():
		pnet, rnet, onet = align.detect_face.create_mtcnn(sess1, None)

graph2 = tf.Graph()

model_exp = 'models/Inception_weights/20170512-110547.pb'
with gfile.FastGFile(model_exp,'rb') as f:
	graph_def = tf.GraphDef()
	graph_def.ParseFromString(f.read())
	graph2 = tf.import_graph_def(graph_def, name='')

sess2 = tf.Session(graph=graph2)

def connect():
	global s
	global host
	global port
	
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.bind((host, port))
	s.listen(5)

def getAttendance(conn, addr):
	data = conn.recv(1024)
	data = data.decode("utf-8")
	print(data)
	
	time.sleep(10)
	
	str = "your path is " + data
	str = str.encode('utf-8')
	conn.send(str)

# def load_model(sess1,sess2):
	# print()


s = None
host = "127.0.0.1"
port = 5001

connect()

while True:
	print("Server started....")
	conn, addr = s.accept()
	with sess2.as_default():
		images_placeholder = tf.get_default_graph().get_tensor_by_name("input:0")
		tf.Print(images_placeholder)

	# t = threading.Thread(target = getAttendance, name = 'thread1', args = (conn, addr))
	# t.start()
	












