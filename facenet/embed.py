import cv2
import tensorflow as tf
import numpy as np
import sys
import os
import glob
# import re
import time
from tensorflow.python.platform import gfile

# import align.detect_face
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

def load_model(model):
	# # Check if the model is a model directory (containing a metagraph and a checkpoint file)
	# #  or if it is a protobuf file with a frozen graph
	model_exp = os.path.expanduser(model)
	if (os.path.isfile(model_exp)):
		print('Model filename: %s' % model_exp)
		with gfile.FastGFile(model_exp,'rb') as f:
			graph_def = tf.GraphDef()
			graph_def.ParseFromString(f.read())
			tf.import_graph_def(graph_def, name='')

def load(path):
	files = glob.glob(os.path.join(path,'*.jpg'))
	img_list = []
	for k,file in enumerate(files):
		img = cv2.imread(file)
		img = img[:,:,::-1]
		img = normalize(img)
		img_list.append(img)
	images = np.asarray(img_list)
	return  images, files

def normalize(x):
	mean = np.mean(x)
	std = np.std(x)
	std_adj = np.maximum(std, 1.0/np.sqrt(x.size))
	y = np.multiply(np.subtract(x, mean), 1/std_adj)
	return y

# def compare(year,dept,div,snapid):
# 	path = os.path.join(year,dept,div)

# 	snapshots_encoding_file = os.path.join(path,'snapshots',snapid,'encodings','encoding.txt')
# 	with open(snapshots_encoding_file,'r') as f:
# 		emb_list = []
# 		label_list = []
# 		for line in f:
# 			line = line.split()
# 			tail, emb = (line[0],line[1:])
# 			emb_list.append(emb)
# 			label_list.append(tail)
# 		snapshots_encoding = np.stack(emb_list)
# 		snapshots_labels = np.stack(label_list)

# 	students_path = os.path.join(path,'students')
# 	files = glob.glob(os.path.join(students_path,'*'))
# 	for k,file in enumerate(files):
# 		_, tail = os.path.split(file)
# 		label_list = []
# 		with open(os.path.join(students_path,tail,'encodings','encodings.txt'),'r') as f:
# 			emb_list = []
# 			label_list.append(tail)
# 			for line in f:
# 				line = line.split()	
# 				tail, emb = (line[0],line[1:])
# 				emb_list.append(emb)
# 			students_encoding = np.stack(emb_list)
# 			dist = np.sqrt(np.sum(np.square(np.subtract(emb[i,:], emb[j,:]))))			


if __name__ == '__main__':

	# year = sys.argv[0]
	# dept = sys.argv[1]
	# div = sys.argv[2]
	# sid = sys.argv[3]								  #/sid is snapshot-id/student-id
	# mode = sys.argv[4]							  #/mode is students/snapshots
	# path = os.path.join(dept,year,div,mode,sid,'tmp')
	start = time.time()
	#Input
	# images_detections, files_detections = load(path)
	#Output
	# out_path = os.path.join(dept,year,div,mode,sid,'encodings')

	model = 'models/Inception_weights/20170512-110547.pb'
	images_detections, files_detections = load('Data/students/Sachin/tmp')
	# for i in range(len(images_candidates)):
	# 	cv2.imshow('window',images_candidates[i])
	# 	cv2.waitKey(0)

	with tf.Graph().as_default():
		# gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.65)
		# sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options, log_device_placement=False))
		# with sess.as_default():
		#Control NUM_THREADS
		# with tf.Session(config=tf.ConfigProto(intra_op_parallelism_threads=NUM_THREADS)) as sess:
		with tf.Session() as sess:
	
			# Load the model
			load_model(model)
	
			# Get input and output tensors
			images_placeholder = tf.get_default_graph().get_tensor_by_name("input:0")
			embeddings = tf.get_default_graph().get_tensor_by_name("embeddings:0")
			phase_train_placeholder = tf.get_default_graph().get_tensor_by_name("phase_train:0")

			# Run forward pass to calculate embeddings
			feed_dict_detections = { images_placeholder: images_detections, phase_train_placeholder:False }
			emb_detections = sess.run(embeddings, feed_dict=feed_dict_detections)
			
			# if(mode=='snapshots'):
			# 	with open(os.path.join(out_path,sid+'.txt'),'w'):
			# 		for k,file in enumerate(files_detections):
			# 			head, tail = os.path.split(file)
			# 			f.write(str(tail)+ " ")
			# 			for j in range(128):
			# 				f.write(str(emb_detections[k,j])+ " ")
			# 			f.write("\n")	
			# elif(mode=='students'):
			# 	with open(os.path.join(out_path,sid+'.txt'),'w'):
			# 		for k,file in enumerate(files_detections):
			# 			head, tail = os.path.split(file)
			# 			f.write(str(tail)+ " ")
			# 			for j in range(128):
			# 				f.write(str(emb_detections[k,j])+ " ")
			# 			f.write("\n")

			with open('Data/students/Sachin/encodings/encoding.txt','w') as f:
				for k,file in enumerate(files_detections):
					head, tail = os.path.split(file)
					f.write(str(tail)+ " ")
					for j in range(128):
						f.write(str(emb_detections[k,j])+ " ")
					f.write("\n")

			end = time.time()
			taken = end - start
			print('Time taken in seconds '+str(taken))


			# for k in range(len(files_detections)):
			# 	for j in range(len(files_candidates)):
			# 		dist = np.sqrt(np.sum(np.square(np.subtract(emb_detections[k,:], emb_candidates[j,:]))))
			# 		print('\n----------------------------')
			# 		print(files_detections[k])
			# 		print(files_candidates[j])
			# 		if(dist>1):
			# 			print('Different persons')
			# 		else:
			# 			print('Same persons')
			# 		img_detection = images_detections[k]
			# 		img_detection = img_detection[:,:,::-1]
			# 		img_candidate = images_candidates[j]
			# 		img_candidate = img_candidate[:,:,::-1]
			# 		cv2.imshow('detections',img_detection)
			# 		cv2.imshow('candidates',img_candidate)
			# 		cv2.waitKey(0)
			
	# dist = np.sqrt(np.sum(np.square(np.subtract(emb[i,:], emb[j,:]))))



