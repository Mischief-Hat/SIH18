# from scipy import misc
import tensorflow as tf
import numpy as np
import sys
import os
import glob
import cv2
import align.detect_face
import sys
import time
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
#Return a glob object
# def load(path):
# 	files = glob.glob(os.path.join(path+'*.jpg'))
# 	return files

if __name__ == '__main__':
	
	# year = sys.argv[0]
	# dept = sys.argv[1]
	# div = sys.argv[2]
	# sid = sys.argv[3]								  #/sid is snapshot-id/student-id
	# mode = sys.argv[4]							  #/mode is students/snapshots
	# path = os.path.join(dept,year,div,mode,sid)
	# files = load(path)                              #/GLOB OBJECT

	files = glob.glob('Data/students/Sachin/photos/*.jpg')
	minsize = 200 # minimum size of face
	threshold = [ 0.6, 0.7, 0.7 ]  # three steps's threshold
	factor = 0.709 # scale factor
	margin = 10

	# if(mode=='students'):
	# 	margin = 10
	# 	minsize = 120
	# elif(mode=='snapshots'):
	# 	margin = 7
	# 	minsize = 50
	start = time.time()

	print('Creating networks and loading parameters')
	with tf.Graph().as_default():
		#Fastest config is on sess#1 with cpu
		gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.65)
		sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options, log_device_placement=False))
		# sess = tf.Session(config=tf.ConfigProto(device_count = {'GPU': 0}, log_device_placement=False))
		with sess.as_default():
			pnet, rnet, onet = align.detect_face.create_mtcnn(sess, None)
	# cv2.namedWindow('faces',cv2.WINDOW_NORMAL)
	# cv2.resizeWindow('faces', 4048,3036)
	
	for k,file in enumerate(files):						#/use glob object returned by load
		img = cv2.imread(os.path.expanduser(file))  	#/ misc.imread(file, mode='RGB')
		img = img[:,:,::-1] #BGR to RGB					#/ Don't use this if using scipy
		
		head, tail = os.path.split(file)
		output = cv2.imread(file)
		img_size = np.asarray(img.shape)[0:2]
		bounding_boxes, _ = align.detect_face.detect_face(img, minsize, pnet, rnet, onet, threshold, factor)
		
		for i in range(len(bounding_boxes)):
			if(i>0):									#/Change mode 
				break
			det = np.squeeze(bounding_boxes[i,0:4])
			bb = np.zeros(4, dtype=np.int32)
			bb[0] = np.maximum(det[0], 0) #Left
			bb[1] = np.maximum(det[1], 0) #Top
			bb[2] = np.minimum(det[2], img_size[1]) #Right
			bb[3] = np.minimum(det[3], img_size[0])	#Bottom
			if(bb[2]-bb[0]<40):
				continue
	
			# cv2.rectangle(output,(bb[0],bb[1]),(bb[2],bb[3]),(0,255,0),2)
			# print(points.shape)
			# for n in range(5):
			# 	x = n
			# 	y = n+5
			# 	cv2.circle(output, (points[x][i],points[y][i]), 1, (0, 255, 0), 4)
			
			faces = output[ bb[1]-margin:bb[3]+margin, bb[0]-margin:bb[2]+margin] #[top:bottom, left:right]			
			print(faces.shape)
			faces = cv2.resize(faces,(160,160))  #Default is bilinear cv2.INTER_LINEAR
			sessid = i+1000
			# dest = 'Data/students/Brijesh/tmp/'+'main-'+str(sessid)+'.jpg'
			dest = 'Data/students/Sachin/tmp/'+tail
			print(i)

			# break
			cv2.imwrite(dest,faces)
			# cv2.imshow('faces',faces)
			# cv2.waitKey(0)
		
		if(k>10):
			end = time.time()
			taken = end - start
			print('Time taken in seconds '+str(taken))
			# exit()
			break
	
		# dest = 'Dataset/cropped/'+tail
		# cv2.imwrite(dest,output)
		# print(dest)
		# cv2.imshow('faces',output)
		# cv2.waitKey(0)
		# cv2.imwrite(dest,output)
