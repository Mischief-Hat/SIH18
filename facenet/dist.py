import os
import sys
import glob
import numpy as np
from sklearn.svm import SVC
import _pickle as pickle

def load_dataset(path):
	students_path = os.path.join(path,'students')
	files = glob.glob(os.path.join(students_path,'*'))
	label_list = []
	emb_list = []
	class_names = []
	for file in files:
		_, tail = os.path.split(file)
		class_names.append(tail)

	for k,file in enumerate(files):
		_, tail = os.path.split(file)
		with open(os.path.join(students_path,tail,'encodings','encoding.txt'),'r') as f:					
			for i,line in enumerate(f):
				emb = np.empty(128, dtype=np.float64)
				line = line.split(' ')	
				for j in range(0,128):
					emb[j] = np.float64(line[j+1])
				emb_list.append(emb)
				for i in range(len(class_names)):
					if(tail==class_names[i]):
						label = i
				label_list.append(label)
	emb_array = np.stack(emb_list)
	# class_names = np.stack(class_names)

	return emb_array, label_list, class_names

def predict():
	#To read classnames
	with open('classifier.pickle','rb') as f:
		_, class_names = pickle.load(f)

	emb_list = []
	label_list = []
	with open('Data/snapshots/sid/encodings/encoding.txt','r') as f:
		for i,line in enumerate(f):
			# if(i==5):
			# 	break
			emb_array = np.empty(128, dtype=np.float64)
			line = line.split(' ')	
			for j in range(0,128):
				emb_array[j] = np.float64(line[j+1])
			emb_list.append(emb_array)
			emb_array = np.asarray(emb_list)
			label_list.append(line[0])
			# for i in range(len(class_names)):
			# 	if(line[0]==class_names[i]):
			# 		label_list.append(i)
			
	print(label_list)
	# label_array = np.asarray(label_list)
	# print(label_array)
	emb_array = emb_array.reshape(-1,128)
	# print(emb_array.shape)

	with open('classifier.pickle','rb') as f:
		model, class_names = pickle.load(f)
		predictions = model.predict_proba(emb_array, )
		# print(predictions)
		print(class_names)
		best_class_indices = np.argmax(predictions, axis=1)
		print(best_class_indices)
		best_class_probabilities = predictions[np.arange(len(best_class_indices)), best_class_indices]
		for i in range(len(best_class_indices)):
			print('%4d  %s: %.3f' % (i, class_names[best_class_indices[i]], best_class_probabilities[i]))

		# accuracy = np.mean(np.equal(best_class_indices, label_array))
		# print('Accuracy: %.3f' % accuracy)

if __name__ == '__main__':
	
	# with open('Data/students/Brijesh/encodings/encoding.txt','r') as f:
	# 	emb_list = []
	#	label_list = []
	# 	#For each entry
	# 	for line in f:
	# 		line = line.split()
	# 		tail, emb = (line[0],line[1:])
	# 		emb_list.append(emb)
	# 	emb_array = np.stack(emb_list)
	# 	print(emb_array.shape)

	# emb_array,label_array, class_names = load_dataset('Data')
	# model = SVC(kernel='linear', probability=True, verbose=True,C=2.0)
	# model.fit(emb_array, label_array)
	# # print(class_names)
	# # print(label_array)

	# with open('classifier.pickle','wb') as f:
	# 	pickle.dump((model, class_names), f)

	predict()



