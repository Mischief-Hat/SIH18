import cv2
import glob
import os
import numpy as np

if __name__ == '__main__':

	files = glob.glob('candidates/main-*')
	for k,file in enumerate(files):

		img = cv2.imread(file)
		res = cv2.resize(img,(160,160))
		cv2.imwrite(file,res)
		print(file)