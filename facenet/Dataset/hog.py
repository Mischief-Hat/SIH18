import cv2
import dlib
import glob
import os

detector = dlib.get_frontal_face_detector()

if __name__ == '__main__':
	
	files = glob.glob('*.jpg')
	for file in files:
		head, tail = os.path.split(file)
		img = cv2.imread(file)

		rect = detector(img)

		for k in range(len(rect)):
			box = rect[k]
			cv2.rectangle(img,(box.left(),box.top()),(box.right(),box.bottom()),(0,255,0),2)
		# dest = head+'../'+'demo/'+tail
		# cv2.imwrite(dest,img)
		# cv2.namedWindow('faces',cv2.WINDOW_NORMAL)
		# cv2.resizeWindow('faces', 4048,3036)
		# cv2.imshow('faces',img)
		# cv2.waitKey(0)