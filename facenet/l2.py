import os
import sys
import glob
import numpy as np

# emb = np.zeros(128, dtype=np.float64)

# with open("Data/students/Brijesh/encodings/encoding.txt","r") as f:
# 	for i,line in enumerate(f):
# 		line = line.split(' ')
# 		file_name = line[0]
# 		for j in range(0,128):
# 			emb[j] = np.float64(line[j+1])

students_path = os.path.join('Data','students')
files = np.asarray(os.listdir(students_path))
label_list = []
emb_list = []
class_names = []

for k,file in enumerate(files):
	_, tail = os.path.split(file)
	with open(os.path.join(students_path,tail,'encodings','encoding.txt'),'r') as f:					
		for i,line in enumerate(f):
			emb = np.empty(128, dtype=np.float64)
			line = line.split(' ')	
			for j in range(0,128):
				emb[j] = np.float64(line[j+1])
			emb_list.append(emb)
	
			# label = np.zeros(len(class_names),dtype=np.int)
			# for i in range(len(class_names)):
			# 	if(tail==class_names[i]):
			# 		label[i] = 1
			label_list.append(label)

emb_array = np.stack(emb_list)
# label_array = np.stack(label_list)
# print(label_array[18])
print(class_names)
# class_names = np.stack(class_names)
	